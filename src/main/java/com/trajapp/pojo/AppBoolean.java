/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.pojo;

import java.io.Serializable;

/**
 *
 * @author Lateefah
 */
public class AppBoolean implements Serializable {
    
    Boolean state;
    
    public AppBoolean() {}
    
    public Boolean getStatus() {
        return state;
    }
    
    public void setStatus(Boolean state) {
        this.state = state;
    }

   
}
