/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author buls
 */
public class UserPayload {

    private int pageSize;
    private int pageNumber;
    private int totalCount;    
    private List<AppUser> users;
    
    public UserPayload() {
        users = new ArrayList<AppUser>();
    }

    public List<AppUser> getUsers() {
        List<AppUser> usersPage = new ArrayList<AppUser>();
                 
        int offset = (pageNumber - 1) * pageSize + 1  ;
        int end = pageNumber*pageSize;
        
        if (offset > users.size()) {
            return usersPage;
        }
        
        if (end > users.size()) {
            end = users.size();
        }
        usersPage = users.subList(offset-1, end);
        
        return usersPage;
    }

    public void setUsers(List<AppUser> users) {
        this.users = users;
    }        

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
    
    
    
}
