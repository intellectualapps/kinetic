/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.pojo;

import com.trajapp.model.Location;
import com.trajapp.model.StayPoint;
import java.util.List;

/**
 *
 * @author buls
 */
public class LocationPayload {
    String id;
    List<StayPoint> stayPoints;
    List<DaySummary> locationPointDaySummary;
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<StayPoint> getStayPoints() {
        return stayPoints;
    }

    public void setStayPoints(List<StayPoint> stayPoints) {
        this.stayPoints = stayPoints;
    }

    public List<DaySummary> getLocationPointDaySummary() {
        return locationPointDaySummary;
    }

    public void setLocationPointDaySummary(List<DaySummary> locationPointDaySummary) {
        this.locationPointDaySummary = locationPointDaySummary;
    }    
        
}
