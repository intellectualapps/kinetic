/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.pojo;

import java.util.List;

/**
 *
 * @author buls
 */
public class LocationPayloadPayload {
    List<LocationPayload> payload;

    public List<LocationPayload> getPayload() {
        return payload;
    }

    public void setPayload(List<LocationPayload> payload) {
        this.payload = payload;
    }    
    
}
