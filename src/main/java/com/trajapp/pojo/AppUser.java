package com.trajapp.pojo;

import java.io.Serializable;
import java.util.Date;

public class AppUser implements Serializable{

    private String userId;
    private String authToken;
    private String userSurname;
    private String userFirstname;
    private String userOthername;
    private String userGender;
    private String userDesignation;
    private String userState;
    private String userLga;
    private String userAddress;
    private String userMobileNo;
    private String userPhoneImei;
    private Date userDate;   
    private String userPassword;

    public AppUser(String userId, String userSurname, String userFirstname,
            String userOthername, String userGender, String userDesignation,
            String userState, String userLga, String userAddress, String userMobileNo,
            String userPhoneImei, Date userDate, String userPassword) {
        this.userId = userId;
        this.userSurname = userSurname;
        this.userFirstname = userFirstname;
        this.userOthername = userOthername;
        this.userGender = userGender;
        this.userDesignation = userDesignation;
        this.userState = userState;
        this.userLga = userLga;
        this.userAddress = userAddress;
        this.userMobileNo = userMobileNo;
        this.userPhoneImei = userPhoneImei;
        this.userDate = userDate;
        this.userPassword = userPassword;
    }

    public AppUser() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserOthername() {
        return userOthername;
    }

    public void setUserOthername(String userOthername) {
        this.userOthername = userOthername;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserDesignation() {
        return userDesignation;
    }

    public void setUserDesignation(String userDesignation) {
        this.userDesignation = userDesignation;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getUserLga() {
        return userLga;
    }

    public void setUserLga(String userLga) {
        this.userLga = userLga;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserPhoneImei() {
        return userPhoneImei;
    }

    public void setUserPhoneId(String userPhoneImei) {
        this.userPhoneImei = userPhoneImei;
    }

    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }
	
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
	
}
