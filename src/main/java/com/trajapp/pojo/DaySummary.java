/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.pojo;

import java.util.List;

/**
 *
 * @author buls
 */
public class DaySummary {
    
    private String date;
    private List<String> daySummary;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getDaySummary() {
        return daySummary;
    }

    public void setDaySummary(List<String> daySummary) {
        this.daySummary = daySummary;
    }        
    
}
