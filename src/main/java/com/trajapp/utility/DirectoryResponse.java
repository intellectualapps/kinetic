package com.trajapp.utility;

//this class holds responses to be sent to the user over http

import java.util.ArrayList;
import java.util.List;

public class DirectoryResponse {
	
	private List<Object> payload;
	private String status;
	private String statusMessage;
	
	//pre: none
	//post: none
	public DirectoryResponse(){
		payload = new ArrayList<Object>();
		status = "";
		statusMessage = "";
	}	

	//pre: none
	//post: none
	public List<Object> getPayload() {
		return payload;
	}

	//pre: none
	//post: payload value set
	public void setPayload(List<Object> payload) {
		this.payload = payload;
	}

	//pre: none
	//post: none
	public String getStatus() {
		return status;
	}

	//pre: none
	//post: status value set
	public void setStatus(String status) {
		this.status = status;
	}

	//pre: none
	//post: none
	public String getStatusMessage() {
		return statusMessage;
	}

	//pre: none
	//post: statusMessage set
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
	
	

}
