package com.trajapp.utility;

public class ResponseCodes {
	
	public static final String OK = "OK";
	public static final String OK_MSG = "OK";
	public static final String FAILED = "FAILED";
	public static final String FAILED_MSG = "FAILED";
	public static final String ERROR_10 = "10";
	public static final String ERROR_10_MSG = "invalid identifier";

}
