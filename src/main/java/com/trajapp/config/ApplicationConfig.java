/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.config;


import com.trajapp.service.LocationService;
import com.trajapp.service.PlacesService;
import com.trajapp.service.UserService;
import com.trajapp.util.exception.GeneralAppExceptionMapper;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;

@javax.ws.rs.ApplicationPath("/api")

public class ApplicationConfig extends Application {
    @Override    
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();
        
        s.add(UserService.class);
        s.add(LocationService.class);
        s.add(PlacesService.class);
        s.add(GeneralAppExceptionMapper.class);
        
        return s;
    }
}
