/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.util;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import java.util.Date;    
import javax.ejb.Stateless;
import com.trajapp.pojo.AppUser;

/**
 *
 * @author Lateefah
 */
@Stateless
public class JWT {    
    SecretKey secretKey = new SecretKey();
    private final String issuer = "humams";
            
    public String createJWT(AppUser user, long ttlMillis) {        
        
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey.getSecret());
        Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

        
        JwtBuilder builder = Jwts.builder()
                                    //.setId(id)
                                    .setIssuedAt(now)
                                    .setSubject(user.getUserId())
                                    .setIssuer(issuer)
                                    .signWith(signatureAlgorithm, signingKey);

        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        return builder.compact();
    }
    
    public Claims parseJWT(String jwt) {
 
        Claims claims = Jwts.parser()         
           .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
           .parseClaimsJws(jwt).getBody();
        
        return claims;
    }        
    
}
