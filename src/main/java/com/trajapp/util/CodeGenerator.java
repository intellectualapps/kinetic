/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.util;

import java.util.Random;
import javax.ejb.Stateless;
import java.security.SecureRandom;
import java.math.BigInteger;

/**
 *
 * @author buls
 */
@Stateless
public class CodeGenerator {
    
    public String getCode() {
        Integer randomCode = new Random().nextInt((99999 - 10000) + 1) + 10000;
        return randomCode.toString();
    }
    

    public String getToken() {
        SecureRandom random = new SecureRandom();    
        return new BigInteger(130, random).toString(32);
    }
    
}