package com.trajapp.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateHandler {

    //pre: none
    //post: none
    public DateHandler() {

    }

    //pre: dateString not null
    //post: none
    public static Date getDate(String dateString) {
        Date date = null;
        if (dateString != null) {
            DateFormat d = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss aa");
            try {
                date = d.parse(dateString);
            } catch (ParseException e) {
            }

            return date;
        } else {
            return null;
        }
    }

    //pre: dateString not null
    //post: none    
    public Date getTime(String timeString) {
        Date time = null;
        if (timeString != null) {
            DateFormat d = new SimpleDateFormat("dd-MMMM-yyyy hh:mm:ss aa");
            try {
                time = d.parse(timeString);
            } catch (ParseException e) {
            }

            return time;
        } else {
            return null;
        }
    }

    public static Timestamp getNow() {
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
        return Timestamp.valueOf(timeStamp);
    }

    public static Timestamp getTimestamp(Date date) {
        return new Timestamp(date.getTime());
    }

    public static Timestamp getTimestamp(String dateString) {
        Date date = getDate(dateString);
        return new Timestamp(date.getTime());
    }

    public static String formatDateInStandardStyle(Calendar calendar) { //Monday, 13 July, 2018        
        String formattedDate = "";
        if (calendar != null) {
            DateFormat formatter = new SimpleDateFormat("EEEE, dd MMMM, yyyy");
            formattedDate = formatter.format(calendar.getTime());            
        }
        return formattedDate;
    }
    
    public static String formatTimeInStandardStyle(Timestamp dateTime) { //1:34:78 pm        
        String formattedTime = "";
        if (dateTime != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(dateTime.getTime());
            DateFormat formatter = new SimpleDateFormat("h:mm:ss aa");
            formattedTime = formatter.format(calendar.getTime());            
        }
        return formattedTime;
    }
}
