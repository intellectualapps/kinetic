/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.util;

/**
 *
 * @author buls
 */
public enum SocialPlatformType {

    EMAIL("email"), FACEBOOK("facebook"), 
    TWITTER("twitter");
    
    String description;

    SocialPlatformType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    
}
