/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.data.manager;

import com.trajapp.model.Location;
import java.sql.Timestamp;
import java.util.List;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Local;

@Local
public interface LocationDataManagerLocal {
    
    Location create(Location location) throws EJBTransactionRolledbackException;
    
    List<Location> getLocationPoints(String id, Timestamp startDate, Timestamp endDate) throws EJBTransactionRolledbackException;

}
