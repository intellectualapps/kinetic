/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.data.manager;

import com.trajapp.util.exception.GeneralAppException;
/**
 *
 * @author Lateefah
 */
public interface ExceptionThrowerManagerLocal {
   
    void throwNullUserAttributesException(String link) throws GeneralAppException;
    
    void throwUserAlreadyExistException(String link) throws GeneralAppException;
    
    void throwEmailAlreadyExistException(String link) throws GeneralAppException;
    
    void throwInvalidEmailFormatException(String link) throws GeneralAppException;
    
    void throwUserDoesNotExistException(String link) throws GeneralAppException;
    
    void throwInvalidLoginCredentialsException(String link) throws GeneralAppException;
    
    void throwInvalidLoginType(String link) throws GeneralAppException;
    
    void throwInvalidEmail(String link) throws GeneralAppException;
    
    void throwInvalidSocialPlatform(String link) throws GeneralAppException;
    
    void throwInvalidTokenException(String link) throws GeneralAppException;
    
    void throwNoEmailLoginAllowedException(String link) throws GeneralAppException;
}
