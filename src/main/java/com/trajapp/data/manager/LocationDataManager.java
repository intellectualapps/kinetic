/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.data.manager;

import com.trajapp.data.provider.DataProviderLocal;
import com.trajapp.model.Location;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class LocationDataManager implements LocationDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public Location create(Location location) throws EJBTransactionRolledbackException {
        return crud.create(location);
    }

    @Override
    public List<Location> getLocationPoints(String id, Timestamp startDate, Timestamp endDate) 
            throws EJBTransactionRolledbackException {
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("locPhoneImei", id);
        parameters.put("startDate", startDate);
        parameters.put("endDate", endDate);
        return crud.findByNamedQuery("Location.findByImeiAndDateRange", parameters, Location.class);
    }

}

