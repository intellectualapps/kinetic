/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.data.manager;

import com.trajapp.data.provider.DataProviderLocal;
import com.trajapp.model.User;
import java.util.HashMap;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBTransactionRolledbackException;
import javax.ejb.Stateless;

@Stateless
public class UserDataManager implements UserDataManagerLocal {

    @EJB
    private DataProviderLocal crud;    

    @Override
    public User create(User user) throws EJBTransactionRolledbackException {
        return crud.create(user);
    }

    @Override
    public User update(User user) {
        return crud.update(user);
    }

    @Override
    public User get(String userId) {
        return crud.find(userId, User.class);
    }

    @Override
    public void delete(User user) {
        crud.delete(user);
    }
  
    @Override
    public List<User> getByEmail(String email) {        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userId", email);
        return crud.findByNamedQuery("User.findByEmail", parameters, User.class);
    }
    
    @Override
    public List<User> getByUsernameFirstNameAndLastName(String searchTerm) {        
        HashMap<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("userId", "%" + searchTerm + "%");
        parameters.put("userFirstname", "%" + searchTerm + "%");
        parameters.put("userSurname", "%" + searchTerm + "%");
        return crud.findByNamedQuery("User.findByUsernameFirstAndLastName", parameters, User.class);
    }
}

