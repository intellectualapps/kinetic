/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.trajapp.data.manager.ExceptionThrowerManagerLocal;
import com.trajapp.data.manager.LocationDataManagerLocal;
import com.trajapp.model.Location;
import com.trajapp.model.StayPoint;
import com.trajapp.pojo.DaySummary;
import com.trajapp.pojo.LocationPayload;
import com.trajapp.pojo.LocationPayloadPayload;
import com.trajapp.util.CodeGenerator;
import com.trajapp.util.DateHandler;
import com.trajapp.util.Verifier;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.trajapp.util.exception.GeneralAppException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Stateless
public class LocationManager implements LocationManagerLocal {

    @EJB
    private LocationDataManagerLocal locationDataManager;

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private CodeGenerator codeGenerator;

    @EJB
    private Verifier verifier;

    private final String SAVE_LOCATION_LINK = "/location";
    private final String GET_LOCATION_POINTS_LINK = "/location/points";
    private final Double DISTANCE_THRESHOLD_METERS = 20.0;
    private final Integer TIME_THRESHOLD_MINUTES = 20;

    @Override
    public boolean saveLocation(String locPhoneId, String locLongitude, String locLatitude, String locAltitude, String locDate, String locTime, String locEstimatedAccuracy) throws GeneralAppException {
        boolean logged = false;
        Location location = new Location(locPhoneId, locLongitude,
                locLatitude, locAltitude, new DateHandler().getTimestamp(locDate),
                new DateHandler().getTimestamp(locTime), locEstimatedAccuracy);
        location.setLocId(codeGenerator.getToken());
        location = locationDataManager.create(location);
        logged = true;
        return logged;
    }

    @Override
    public LocationPayloadPayload getLocationPoints(String ids, String startDate, String endDate, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(GET_LOCATION_POINTS_LINK).verifyParams(ids, startDate, endDate, rawToken);        
        verifier.setResourceUrl(GET_LOCATION_POINTS_LINK).verifyJwt(rawToken);
        
        Timestamp fromDate = DateHandler.getTimestamp(startDate);
        Timestamp toDate = DateHandler.getTimestamp(endDate);
        
        List<String> allIds = getIds(ids);
        
        List<LocationPayload> locationPayloads = new ArrayList<LocationPayload>();
        for (String id : allIds) {

            List<Location> locationPoints = locationDataManager.getLocationPoints(id, fromDate, toDate);
            List<StayPoint> stayPoints = getStayPoints(locationPoints);
            List<DaySummary> locationPointsDaySummary = getStayPointsByDay(stayPoints);
            LocationPayload locationPayload = new LocationPayload();
            locationPayload.setId(id);
            locationPayload.setStayPoints(stayPoints);
            locationPayload.setLocationPointDaySummary(locationPointsDaySummary);
            locationPayloads.add(locationPayload);
        }
        
        LocationPayloadPayload payload = new LocationPayloadPayload();
        payload.setPayload(locationPayloads);
        
        return payload;
    }
    
    private List<StayPoint> getStayPoints(List<Location> locationPoints) {
        List<StayPoint> stayPoints = new ArrayList<StayPoint>();
        int j = 0;
        for (int i = 0; i < locationPoints.size(); i++) {
            j = i + 1;
            if (j < locationPoints.size()) {
                Location locationPoint1 = locationPoints.get(i);
                Location locationPoint2 = locationPoints.get(j);
                Double locationPoint1Lat = Double.parseDouble(locationPoint1.getLocLatitude());
                Double locationPoint1Lon = Double.parseDouble(locationPoint1.getLocLongitude());
                Double locationPoint1Alt = Double.parseDouble(locationPoint1.getLocAltitude());
                Double locationPoint2Lat = Double.parseDouble(locationPoint2.getLocLatitude());
                Double locationPoint2Lon = Double.parseDouble(locationPoint2.getLocLongitude());
                Double locationPoint2Alt = Double.parseDouble(locationPoint2.getLocAltitude());
                
                Double distanceBetweenPoints = getDistanceBetweenPoints(locationPoint1Lat, locationPoint2Lat, 
                        locationPoint1Lon, locationPoint2Lon, locationPoint1Alt, locationPoint2Alt);
                if (distanceBetweenPoints > DISTANCE_THRESHOLD_METERS) {
                    Long timeDifference = locationPoint2.getLocDate().getTime() - locationPoint1.getLocDate().getTime();
                    Long timeDifferenceInMinutes = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
                    if (timeDifferenceInMinutes > TIME_THRESHOLD_MINUTES) {
                        System.out.println("Point 1 Lat: " + locationPoint1Lat + " Lon: " + locationPoint1Lon);
                        System.out.println("Point 2 Lat: " + locationPoint2Lat + " Lon: " + locationPoint2Lon);
                        Location centerPoint = getCenterOfPoints(locationPoint1Lat, locationPoint1Lon, locationPoint2Lat, locationPoint2Lon);
                        System.out.println("Distance between points: " + distanceBetweenPoints);
                        StayPoint stayPoint = new StayPoint();
                        stayPoint.setLocId(codeGenerator.getToken());
                        stayPoint.setLocPhoneImei(locationPoint1.getLocPhoneImei());
                        stayPoint.setLocLatitude(centerPoint.getLocLatitude());
                        stayPoint.setLocLongitude(centerPoint.getLocLongitude());
                        stayPoint.setLocDuration(timeDifference);
                        System.out.println("duration in minutes: " + timeDifferenceInMinutes);
                        stayPoint.setLocStartDateTime(locationPoint1.getLocDate());
                        stayPoint.setLocEndDateTime(locationPoint2.getLocDate());
                        stayPoint.setLocEstimatedAccuracy("0");
                        stayPoint.setLocAltitude("0");
                        
                        stayPoints.add(stayPoint);
                    }
                }
            }
        }
        
        return stayPoints;
    }
    
    public static double getDistanceBetweenPoints(double latPoint1, double latPoint2, double lonPoint1,
            double lonPoint2, double elPoint1, double elPoint2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(latPoint2 - latPoint1);
        double lonDistance = Math.toRadians(lonPoint2 - lonPoint1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(latPoint1)) * Math.cos(Math.toRadians(latPoint2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = elPoint1 - elPoint2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance);
    }
    
    public static Location getCenterOfPoints(double latPoint1, double lonPoint1, double latPoint2, double lonPoint2) {

        double dLon = Math.toRadians(lonPoint2 - lonPoint1);

        //convert to radians
        latPoint1 = Math.toRadians(latPoint1);
        latPoint2 = Math.toRadians(latPoint2);
        lonPoint1 = Math.toRadians(lonPoint1);

        double Bx = Math.cos(latPoint2) * Math.cos(dLon);
        double By = Math.cos(latPoint2) * Math.sin(dLon);
        double lat3 = Math.atan2(Math.sin(latPoint1) + Math.sin(latPoint2), Math.sqrt((Math.cos(latPoint1) + Bx) * (Math.cos(latPoint1) + Bx) + By * By));
        double lon3 = lonPoint1 + Math.atan2(By, Math.cos(latPoint1) + Bx);

        //print out in degrees
        Double centerLatitude = Math.toDegrees(lat3);
        Double centerLongitude = Math.toDegrees(lon3);
        System.out.println("Center point: " + centerLatitude + " " + centerLongitude);
        Location point = new Location();
        point.setLocLatitude(centerLatitude.toString());
        point.setLocLongitude(centerLongitude.toString());
        
        return point;
    }
    
    private List<String> getIds(String ids) {
        List<String> allIds = new ArrayList<String>();
        if (ids.contains(",")) {
            String[] idStrings = ids.split(",");
            for (int i = 0; i < idStrings.length; i++) {
                allIds.add(idStrings[i]);
            }
        } else {
            allIds.add(ids);
        }
        
        return allIds;
    }
    
    private List<DaySummary> getLocationPointsByDay(List<Location> locationPoints) {
        HashMap<String, List<String>> pointsByDays = new HashMap<String, List<String>>();
        for (Location location : locationPoints) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(location.getLocDate().getTime());
            String formattedDate = DateHandler.formatDateInStandardStyle(calendar);
            String durationBetweenPoints = getDurationBetweenPoints(locationPoints, locationPoints.indexOf(location));
            
            if (pointsByDays.containsKey(formattedDate)) {
                List<String> points = pointsByDays.get(formattedDate);
                points.add(DateHandler.formatTimeInStandardStyle(location.getLocTime()) + " - " + location.getLocId() + " - " +durationBetweenPoints);
                pointsByDays.put(formattedDate, points);
            } else {
                List<String> points = new ArrayList<String>();
                points.add(DateHandler.formatTimeInStandardStyle(location.getLocTime()) + " - " + location.getLocId() + " - " +durationBetweenPoints);
                pointsByDays.put(formattedDate, points);
            }
        }
        
        List<DaySummary> daySummaries = new ArrayList<DaySummary>();
        Set<String> keySet = pointsByDays.keySet();
        for (String key : keySet) {
            DaySummary daySummary = new DaySummary();
            daySummary.setDate(key);
            
            System.out.println(key);
            
            List<String> points = pointsByDays.get(key);
            daySummary.setDaySummary(points);
            daySummaries.add(daySummary);
            for (String point : points) {
                System.out.println(point);
            }
        }
        
        return daySummaries;
    }
    
    private List<DaySummary> getStayPointsByDay(List<StayPoint> stayPoints) {
        HashMap<String, List<String>> pointsByDays = new HashMap<String, List<String>>();
        for (StayPoint stayPoint : stayPoints) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(stayPoint.getLocStartDateTime().getTime());
            String formattedDate = DateHandler.formatDateInStandardStyle(calendar);
            String duration = getFormattedDuration(stayPoint.getLocDuration());
            
            if (pointsByDays.containsKey(formattedDate)) {
                List<String> points = pointsByDays.get(formattedDate);
                points.add(DateHandler.formatTimeInStandardStyle(stayPoint.getLocStartDateTime()) + " - " + stayPoint.getLocId() + " - " +duration);
                pointsByDays.put(formattedDate, points);
            } else {
                List<String> points = new ArrayList<String>();
                points.add(DateHandler.formatTimeInStandardStyle(stayPoint.getLocStartDateTime()) + " - " + stayPoint.getLocId() + " - " +duration);
                pointsByDays.put(formattedDate, points);
            }
        }
        
        List<DaySummary> daySummaries = new ArrayList<DaySummary>();
        Set<String> keySet = pointsByDays.keySet();
        for (String key : keySet) {
            DaySummary daySummary = new DaySummary();
            daySummary.setDate(key);
            
            System.out.println(key);
            
            List<String> points = pointsByDays.get(key);
            daySummary.setDaySummary(points);
            daySummaries.add(daySummary);
            for (String point : points) {
                System.out.println(point);
            }
        }
        
        return daySummaries;
    }

    private String getDurationBetweenPoints(List<Location> locations, int seedIndex) {
        String duration = "0 secs";
        Location seedLocation = locations.get(seedIndex);
        int nextIndex = seedIndex + 1;
        if (nextIndex >= locations.size()) {
            return duration;
        }
        Location nextLocation = locations.get(seedIndex + 1);
        if (nextLocation != null) {
            long seedDate = seedLocation.getLocDate().getTime();
            long nextDate = nextLocation.getLocDate().getTime();
            long difference = nextDate - seedDate;
            
            Long minutes = TimeUnit.MILLISECONDS.toMinutes(difference);
            Long hours = TimeUnit.MILLISECONDS.toHours(difference);
            Long days = TimeUnit.MILLISECONDS.toDays(difference);
            if (!(days < 1l)) {
                if (days == 1) {
                    duration = days.toString() + " day";
                } else {
                    duration = days.toString() + " days";
                }                
                return duration;
            } else if (!(hours < 1l)) {
                if (hours == 1) {
                    duration = hours.toString() + " hour";
                } else {
                    duration = hours.toString() + " hours";
                }
                return duration;
            } else if (!(minutes < 1l)) {
                if (minutes == 1) {
                    duration = minutes.toString() + " minute";
                } else {
                    duration = minutes.toString() + " minutes";
                }
                return duration;
            }
        }
        
        return duration;
    }
    
    private String getFormattedDuration(Long durationInMiliseconds) {
        Long minutes = TimeUnit.MILLISECONDS.toMinutes(durationInMiliseconds);
        Long hours = TimeUnit.MILLISECONDS.toHours(durationInMiliseconds);
        Long days = TimeUnit.MILLISECONDS.toDays(durationInMiliseconds);
        
        String duration = "0 secs";
        if (!(days < 1l)) {            
            if (days == 1) {
                duration = ((Integer)days.intValue()).toString() + " day";
            } else {
                duration = ((Integer)days.intValue()).toString() + " days";
            }
            return duration;
        } else if (!(hours < 1l)) {
            if (hours == 1) {
                duration = ((Integer)hours.intValue()).toString() + " hour";
            } else {
                duration = ((Integer)hours.intValue()).toString() + " hours";
            }
            return duration;
        } else if (!(minutes < 1l)) {            
            if (minutes == 1) {
                duration = ((Integer)minutes.intValue()).toString() + " minute";
            } else {
                duration = ((Integer)minutes.intValue()).toString() + " minutes";
            }
            return duration;
        }
        
        return duration;
    }
    
}
