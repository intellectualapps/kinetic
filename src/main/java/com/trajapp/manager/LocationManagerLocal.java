/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.trajapp.pojo.LocationPayloadPayload;
import com.trajapp.util.exception.GeneralAppException;

public interface LocationManagerLocal {
    
    boolean saveLocation(String locPhoneId, String locLongitude,
			String locLatitude, String locAltitude, String locDate,
			String locTime, String locEstimatedAccuracy) throws GeneralAppException;
    
    LocationPayloadPayload getLocationPoints(String ids, String startDate, String endDate, String rawToken) throws GeneralAppException;
}