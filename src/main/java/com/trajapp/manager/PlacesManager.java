/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;
import com.trajapp.data.manager.ExceptionThrowerManagerLocal;
import com.trajapp.data.manager.LocationDataManagerLocal;
import com.trajapp.util.CodeGenerator;
import com.trajapp.util.Verifier;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.trajapp.util.exception.GeneralAppException;
import java.io.IOException;

@Stateless
public class PlacesManager implements PlacesManagerLocal {

    @EJB
    private LocationDataManagerLocal locationDataManager;

    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;

    @EJB
    private CodeGenerator codeGenerator;

    @EJB
    private Verifier verifier;

    private final String PLACES_LINK = "/places";    

    @Override
    public String getPlaces(String latitude, String longitude) throws GeneralAppException {
        GeoApiContext context = new GeoApiContext.Builder()
                .apiKey("AIzaSyDE7rz5UpIshE5Z227CM5KTTLwSSnXBwGI")
                .build();
        verifier.setResourceUrl(PLACES_LINK).verifyParams(latitude, longitude);
        
        Double lat = Double.parseDouble(latitude);
        Double lon = Double.parseDouble(longitude);
        
        String places = "";
        LatLng location = new LatLng(lat, lon);
        try {
            PlacesSearchResponse searchResponse = PlacesApi.nearbySearchQuery(context, location)
                    .radius(100)
                    .await();
            PlacesSearchResult[] results = searchResponse.results;
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            places = gson.toJson(results);
            for (int i = 0; i < results.length; i++) {
                System.out.println(gson.toJson(results[i]));                
            }
            
        } catch (ApiException ae) {
            System.out.println("APIEXCEPTION: " + ae.getMessage());
        } catch (InterruptedException ie) {
            System.out.println("INTERRUPTEDEXCEPTION: " + ie.getMessage());
        } catch (IOException ioe) {
            System.out.println("IOEXCEPTION: " + ioe.getMessage());
        }
        
        return places;
    }

}
