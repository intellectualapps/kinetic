/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.trajapp.data.manager.ExceptionThrowerManagerLocal;
import com.trajapp.data.manager.UserDataManagerLocal;
import com.trajapp.model.User;
import com.trajapp.pojo.AppBoolean;
import com.trajapp.pojo.AppUser;
import com.trajapp.pojo.UserPayload;
import com.trajapp.util.CodeGenerator;
import com.trajapp.util.JWT;
import com.trajapp.util.MD5;
import com.trajapp.util.SocialPlatformType;
import com.trajapp.util.Verifier;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.trajapp.util.exception.GeneralAppException;
import java.util.ArrayList;

/**
 *
 * @author Lateefah
 */
@Stateless
public class UserManager implements UserManagerLocal {

    @EJB
    private UserDataManagerLocal userDataManager;
    
    @EJB
    private ExceptionThrowerManagerLocal exceptionManager;
    
    @EJB
    private CodeGenerator codeGenerator;
    
    @EJB
    private Verifier verifier;

    private final String USER_LINK = "/user";
    private final String AUTHENTICATE_USER_LINK = "/user/authenticate";
    private final String VERIFY_USERNAME_LINK = "/user/verify";
    private final String CREATE_USERNAME_LINK = "/user/username";
    private final String LINK_SOCIAL_ACCOUNT = "/user/link-social";
    private final String UNLINK_SOCIAL_ACCOUNT = "/user/unlink-social";    
    private final String NOTIFY_OFFLINE_USER_LINK = "/message/notify";
    private final String DEVICE_TOKEN_LINK = "/user/device-token";
    private final long TOKEN_LIFETIME= 31556952000l;

          
    @Override
    public AppUser register(String email, String firstName, String surname, 
            String phoneNumber, String password) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(email, firstName, surname, phoneNumber, password);        
        
        if (!isValidEmailAddress(email)) {
            exceptionManager.throwInvalidEmailFormatException(USER_LINK);
        }                
        
        if (userDataManager.get(email) != null) {
            exceptionManager.throwUserAlreadyExistException(USER_LINK);
        }
        
        if (!userDataManager.getByEmail(email).isEmpty()) { 
            exceptionManager.throwEmailAlreadyExistException(email);
        }

        User user = new User();

        user.setUserId(email);
        user.setUserMobileNo(phoneNumber);
        user.setUserFirstname(firstName);
        user.setUserSurname(surname);
        user.setUserPassword(MD5.hash(password));
        
        user = userDataManager.create(user);
        AppUser appUser = getAppUser(user);
        appUser = getAppUserWithToken(appUser);        
           
        return appUser;
    }
    
    @Override
    public AppUser updateUserDetails(String username, String email, String firstName, 
            String lastName, String phoneNumber, String photoUrl, String location, 
            String facebookEmail, String twitterEmail, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);
        
        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        User user = userDataManager.get(username);
        
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        if (firstName != null && !firstName.isEmpty()) {
            user.setUserFirstname(firstName);
        }
        if (lastName != null && !lastName.isEmpty()) {
            user.setUserSurname(lastName);
        }
        if (phoneNumber != null && !phoneNumber.isEmpty()) {
            user.setUserMobileNo(phoneNumber);
        }
              
    
        return getAppUser(userDataManager.update(user));
    }
    
    @Override
    public AppBoolean deleteUser(String username) throws GeneralAppException {

        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username);

        User user = userDataManager.get(username);
        if (user == null) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        //catch database errors here
        userDataManager.delete(user);
        
        return getAppBoolean(true);
    }
    
    @Override
    public AppUser getUserDetails (String username, String rawToken) throws GeneralAppException {
        
        verifier.setResourceUrl(USER_LINK)
                .verifyParams(username, rawToken);
        
        verifier.setResourceUrl(USER_LINK)
                .verifyJwt(rawToken);
        
        AppUser appUser = null;
        
        try {
            User user = userDataManager.get(username);
            if (user != null) {
                appUser = getAppUser(user);
            }
        } catch (Exception e) {
            exceptionManager.throwUserDoesNotExistException(USER_LINK);
        }
        
        return appUser;
    }        
     
    @Override
    public AppUser authenticate (String id, String password, String type) throws GeneralAppException {
        
        verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(type);
        
        AppUser appUser = null;
        if (type.equals(SocialPlatformType.EMAIL.getDescription())) {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id, password);
        } else {
            verifier.setResourceUrl(AUTHENTICATE_USER_LINK)
                    .verifyParams(id);
        }
       
        User usernameUser = userDataManager.get(id);

        if (usernameUser == null) {
            exceptionManager.throwUserDoesNotExistException(AUTHENTICATE_USER_LINK);
        }

        if (usernameUser.getUserPassword() == null || usernameUser.getUserPassword().isEmpty()) {
            exceptionManager.throwNoEmailLoginAllowedException(AUTHENTICATE_USER_LINK);
        }

        if (!(usernameUser != null && usernameUser.getUserPassword().equals(MD5.hash(password)))) {
            exceptionManager.throwInvalidLoginCredentialsException(AUTHENTICATE_USER_LINK);
        }
        appUser = getAppUserWithToken(getAppUser(usernameUser));

        return appUser;
    }
    
    @Override
    public AppBoolean verifyUsername(String username) throws GeneralAppException{
        verifier.setResourceUrl(VERIFY_USERNAME_LINK)
                .verifyParams(username);
        
        User user = userDataManager.get(username);
        if (user == null ) {
            return getAppBoolean(true);
        }
        return getAppBoolean(false);
    }
    
    private List<AppUser> getAppUsers(List<User> users) {
        List<AppUser> appUsers = new ArrayList<AppUser>();
        for (User user : users) {
            AppUser appUser = getAppUser(user);
            appUsers.add(appUser);            
        }
        
        return appUsers;
    }
    
    private AppUser getAppUser(User user) {        
        AppUser appUser = new AppUser();
        if (user != null) {
            appUser.setUserId(user.getUserId());            
            appUser.setUserFirstname(user.getUserFirstname() == null ? "" : user.getUserFirstname());
            appUser.setUserSurname(user.getUserSurname() == null ? "" : user.getUserSurname());
            appUser.setUserMobileNo(user.getUserMobileNo() == null ? "" : user.getUserMobileNo());
            appUser.setUserDate(user.getUserDate());            
        }
        return appUser;
    }
        
    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT token = new JWT();      
        appUser.setAuthToken(token.createJWT(appUser, TOKEN_LIFETIME));
        return appUser;
    }
    
    private AppBoolean getAppBoolean(Boolean status) {
        AppBoolean appBoolean = new AppBoolean();
        
        appBoolean.setStatus(status);
        
        return appBoolean;
    }
            
    private boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    @Override
    public UserPayload getListOfUsers(String usernames, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(usernames, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);
        
        List<AppUser> usersDetails = new ArrayList<AppUser>();
        System.out.println("USERNAMES: " + usernames);
        String[] listOfUsernames = usernames.split("\\s*,\\s*");
        for (int i = 0; i < listOfUsernames.length; i++) {
            String username = listOfUsernames[i];
            if (username != null && !username.isEmpty()) {
                AppUser appUser = getUserDetails(username, rawToken);
                if (appUser != null) {
                    System.out.println("ADDING USER ID: " + appUser.getUserId());
                    usersDetails.add(appUser);
                }
            }
        }
        
        UserPayload users = new UserPayload();
        users.setPageNumber(1);
        users.setPageSize(listOfUsernames.length);
        users.setUsers(usersDetails);
        
        return users;
    }
    
    @Override
    public UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException {
        verifier.setResourceUrl(USER_LINK).verifyParams(searchTerm, rawToken);
        verifier.setResourceUrl(USER_LINK).verifyJwt(rawToken);
        
        List<User> users = userDataManager.getByUsernameFirstNameAndLastName(searchTerm);
        
        List<AppUser> appUsers = getAppUsers(users);
        
        UserPayload usersPayload = new UserPayload();
        usersPayload.setUsers(appUsers);
        usersPayload.setPageNumber(Integer.parseInt(pageNumber));
        usersPayload.setPageSize(Integer.parseInt(pageSize));
        usersPayload.setTotalCount(appUsers.size());
        
        
        return usersPayload;
    }
   
}
