/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.trajapp.pojo.AppBoolean;
import com.trajapp.pojo.AppUser;
import com.trajapp.pojo.UserPayload;
import com.trajapp.util.exception.GeneralAppException;

public interface UserManagerLocal {
    
    AppUser getUserDetails (String username, String rawToken) throws GeneralAppException; 
    
    AppUser register(String email, String firstName, String surname, 
            String phoneNumber, String password) throws GeneralAppException;
    
    AppUser updateUserDetails(String username, String email, String firstName, String lastName,
                                String phoneNumber, String photoUrl, String location, String facebookEmail,
                                String twitterEmail, String rawToken) throws GeneralAppException;
    
    AppBoolean deleteUser(String username) throws GeneralAppException;
    
    AppUser authenticate (String id, String password, String type) throws GeneralAppException;
    
    AppBoolean verifyUsername(String username) throws GeneralAppException;
            
    UserPayload getListOfUsers (String usernames, String rawToken) throws GeneralAppException;
            
    UserPayload searchUsers(String searchTerm, String pageNumber, String pageSize, String rawToken) throws GeneralAppException;
}