/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.manager;

import com.trajapp.util.exception.GeneralAppException;

public interface PlacesManagerLocal {
    
    String getPlaces(String latitude, String longitude) throws GeneralAppException;
    
}