package com.trajapp.manager;

//this class is used to handle all device specific actions

/*import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.text.DateFormat;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import com.trajapp.model.Location;
import com.trajapp.model.Smartphone;
import com.trajapp.persistence.PMF;
import com.trajapp.utility.DateHandler;
import com.trajapp.utility.DirectoryResponse;
*/
public class DeviceManager {
	
/*PersistenceManager pm = null;
	
    //pre: none
    //post: pm instantiated
	public DeviceManager(){
		this.pm = PMF.get().getPersistenceManager();
	}

	//pre: phoneId, phoneName, phoneUserId, phoneInvoiceId, 
	//     phoneMake, phoneModel, phoneColor, phoneDate not null
	//post: device details saved to database
	public boolean createDevice(String phoneId, String phoneName, String phoneUserId,
			String phoneInvoiceId, String phoneMake, String phoneModel,
			String phoneColor, String phoneDate){
		
		boolean created = false;
		Smartphone phone = new Smartphone(phoneId, phoneName, phoneUserId,
				phoneInvoiceId, phoneMake, phoneModel,
				phoneColor, new DateHandler().getDate(phoneDate));
		try{
			pm.makePersistent(phone);
			created = true;
		} finally{
			pm.close();
		}
		return created;
	}

	//pre: none
	//post: none
	public List<Smartphone> getRegisteredDevices(){
		List<Smartphone> devices = new ArrayList<Smartphone>();
		
		Query q = pm.newQuery(Smartphone.class);
        List<Smartphone> registeredDevices = (List<Smartphone>)q.execute();
        if(registeredDevices.isEmpty()){
        	
        } else{
        	for(Smartphone device : registeredDevices){
        		devices.add(pm.detachCopy(device));
        	}
        }
				
		return devices;
	}

	//pre: locPhoneImei not null
	//post: none
	public boolean isRegisteredDevice(String locPhoneImei) {
		boolean isRegistered = false;
		List<Smartphone> devices = getRegisteredDevices();
		
		for(Smartphone device : devices){
			if(locPhoneImei.equals(device.getPhoneImei())){
				isRegistered = true;
			}
    	}
		return true;
	}
	
*/	
}
