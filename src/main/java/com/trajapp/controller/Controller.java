package com.trajapp.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*import javax.jdo.PersistenceManager;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;*/
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*import com.trajapp.manager.DeviceManager;
import com.trajapp.manager.LocationManager;
import com.trajapp.manager.UserManager;
import com.trajapp.persistence.PMF;
import com.trajapp.utility.DirectoryResponse;
import com.trajapp.utility.ResponseCodes;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
*/

public class Controller  extends HttpServlet{
	
//	PersistenceManager pm;
//	PrintWriter out;
//	Gson gsonCreator;
	
	//services
	public static String INIT = "init"; //register and setup a mobile with necessary parameters
	public static String LOGCOORDINATES = "log"; //saves location details received from a mobile client
	
	//pre: none
	//post: pm, gsonCreator instantiated
	/*@Override
	public void init() throws ServletException {
	    pm = PMF.get().getPersistenceManager();	   
	    gsonCreator = new GsonBuilder().setPrettyPrinting().create();
	}*/
	
	//pre: request, response not null
	//post: none
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
						
	/*	String action = request.getParameter("action");
		out = response.getWriter();
		
		if(action.equals(INIT)){
			DirectoryResponse dr = new DirectoryResponse();
			//get the identifier sent
			String imei = request.getParameter("imei");
			//confirm identifier is in permitted list
			boolean exists = true; //call the manager and confirm existence here
			if(exists){
				//if it is then fetch all necessary parameters and send to mobile client
				
				dr.setStatus(ResponseCodes.OK);
	        	dr.setStatusMessage(ResponseCodes.OK_MSG);
	        	List<Object> interval = new ArrayList<Object>();
	        	interval.add("30000");
	        	dr.setPayload(interval);	
			} else { //otherwise return with an error
				dr.setStatus(ResponseCodes.ERROR_10);
	        	dr.setStatusMessage(ResponseCodes.ERROR_10_MSG);
			}
			
        	String json = gsonCreator.toJson(dr);
            out.write(json);
			
		} else if(action.equals(LOGCOORDINATES)){
			DirectoryResponse dr = new DirectoryResponse();
			//confirm identifier is permitted
			String locPhoneImei = request.getParameter("lphoneimei");
			DeviceManager dm = new DeviceManager();
			if(dm.isRegisteredDevice(locPhoneImei)){
				//retrieve parameters neccessary for logging coordinates
				//String locId = request.getParameter("lid");			
							
				String locLongitude = request.getParameter("llong");			
				String locLatitude = request.getParameter("llat");			
				String locAltitude = request.getParameter("lalt");			
				String locDate = request.getParameter("ldate");			
				String locTime = request.getParameter("ltime");			
				String locEstimatedAccuracy = request.getParameter("lacc");
				
				boolean logged = new LocationManager().saveLocation(locPhoneImei, locLongitude, 
						locLatitude, locAltitude, locDate, locTime, locEstimatedAccuracy);
				//send a json response
				
	            if(logged){            	
	            	dr.setStatus(ResponseCodes.OK);
	            	dr.setStatusMessage(ResponseCodes.OK_MSG);				
				} else{
					dr.setStatus(ResponseCodes.FAILED);
					dr.setStatusMessage(ResponseCodes.FAILED_MSG);
				}
			} else{				
	            dr.setStatus(ResponseCodes.ERROR_10);
				dr.setStatusMessage(ResponseCodes.ERROR_10_MSG);				
			}
			
            String json = gsonCreator.toJson(dr);
            out.write(json);
		} else if(action.equals("buildobjects")){
			new UserManager().createUser("aa", "aa", "aa", "aa", "male", 
					"aa", "aa", "aa", "aa", "", "", "01-01-2013");
			new LocationManager().saveLocation("pa", "0.0",
					"0.0", "0", "01-01-2013",
					"01-01-2013", "0");
			new DeviceManager().createDevice("pa", "aa", "aa", "aa", "aa", "aa",
			        "aa", "01-01-2013");
		} else if(action.equals("adduser")){
			String userId = request.getParameter("uid");			
			String userSurname = request.getParameter("usurname");			
			String userFirstname = request.getParameter("ufirstname");			
			String userOthername = request.getParameter("uothername");			
			String userGender = request.getParameter("ugender");			
			String userDesignation = request.getParameter("udesignation");			
			String userState = request.getParameter("ustate");			
			String userLga = request.getParameter("ulga");			
			String userAddress = request.getParameter("uaddress");			
			String userMobileNo = request.getParameter("umobileno");			
			String userPhoneImei = request.getParameter("uphoneimei");			
			String userDate = request.getParameter("udate");
			
			boolean created = new UserManager().createUser(userId, userSurname, userFirstname, 
					userOthername, userGender, userDesignation, userState, 
					userLga, userAddress, userMobileNo, userPhoneImei, userDate);
			//send a json response
			DirectoryResponse dr = new DirectoryResponse();
            if(created){            	
            	dr.setStatus(ResponseCodes.OK);
            	dr.setStatusMessage(ResponseCodes.OK_MSG);				
			} else{
				dr.setStatus(ResponseCodes.FAILED);
				dr.setStatusMessage(ResponseCodes.FAILED_MSG);
			}
            String json = gsonCreator.toJson(dr);
            out.write(json);
		} else if(action.equals("addsmartphone")){
			String phoneImei = request.getParameter("pimei");			
			String phoneName = request.getParameter("pname");			
			String phoneUserId = request.getParameter("puserid");			
			String phoneInvoiceId = request.getParameter("pinvoiceid");			
			String phoneMake = request.getParameter("pmake");			
			String phoneModel = request.getParameter("pmodel");			
			String phoneColor = request.getParameter("pcolor");
			String phoneDate = request.getParameter("pdate");
			
			boolean created = new DeviceManager().createDevice(phoneImei, phoneName, phoneUserId, 
					phoneInvoiceId, phoneMake, phoneModel, phoneColor, phoneDate);
			//send a json response
			DirectoryResponse dr = new DirectoryResponse();
            if(created){            	
            	dr.setStatus(ResponseCodes.OK);
            	dr.setStatusMessage(ResponseCodes.OK_MSG);				
			} else{
				dr.setStatus(ResponseCodes.FAILED);
				dr.setStatusMessage(ResponseCodes.FAILED_MSG);
			}
            String json = gsonCreator.toJson(dr);
            out.write(json);
		} else if(action.equals("loglocation")){
			
		}
		*/
		
				
	}

	//pre: none
	//post: none
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	/*	String action = request.getParameter("action");
		out = response.getWriter();
		
		if(action.equals("login")){
                    String userId = new UserManager().login("", "");
                    String json = gsonCreator.toJson(userId);
                    out.write(json);
                }
                */
	}

}
