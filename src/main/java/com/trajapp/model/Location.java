package com.trajapp.model;


import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "location")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "Location.findByImeiAndDateRange", query = "SELECT l FROM Location l WHERE l.locPhoneImei = :locPhoneImei AND l.locDate BETWEEN :startDate AND :endDate ORDER BY l.locDate ASC")})
public class Location {

    @Id
    @NotNull
    @Column(name = "locId")
    private String locId;    
    
    @NotNull
    @Column(name = "locPhoneImei")
    private String locPhoneImei;    
    
    @NotNull
    @Column(name = "locLongitude")
    private String locLongitude;
    
    @NotNull
    @Column(name = "locLatitude")
    private String locLatitude;
    
    @NotNull
    @Column(name = "locAltitude")
    private String locAltitude;
    
    @NotNull
    @Column(name = "locDate")
    private Timestamp locDate;
    
    @NotNull
    @Column(name = "locTime")
    private Timestamp locTime;
    
    @NotNull
    @Column(name = "locEstimateAccuracy")
    private String locEstimatedAccuracy;

    public Location() {

    }

    public Location(String locPhoneImei, String locLongitude,
            String locLatitude, String locAltitude, Timestamp locDate,
            Timestamp locTime, String locEstimatedAccuracy) {

        this.locPhoneImei = locPhoneImei;
        this.locLongitude = locLongitude;
        this.locLatitude = locLatitude;
        this.locAltitude = locAltitude;
        this.locDate = locDate;
        this.locTime = locTime;
        this.locEstimatedAccuracy = locEstimatedAccuracy;
    }

    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    public String getLocPhoneImei() {
        return locPhoneImei;
    }

    public void setLocPhoneImei(String locPhoneImei) {
        this.locPhoneImei = locPhoneImei;
    }

    public String getLocLongitude() {
        return locLongitude;
    }

    public void setLocLongitude(String locLongitude) {
        this.locLongitude = locLongitude;
    }

    public String getLocLatitude() {
        return locLatitude;
    }

    public void setLocLatitude(String locLatitude) {
        this.locLatitude = locLatitude;
    }

    public String getLocAltitude() {
        return locAltitude;
    }

    public void setLocAltitude(String locAltitude) {
        this.locAltitude = locAltitude;
    }

    public Timestamp getLocDate() {
        return locDate;
    }

    public void setLocDate(Timestamp locDate) {
        this.locDate = locDate;
    }

    public Timestamp getLocTime() {
        return locTime;
    }

    public void setLocTime(Timestamp locTime) {
        this.locTime = locTime;
    }

    public String getLocEstimatedAccuracy() {
        return locEstimatedAccuracy;
    }

    public void setLocEstimatedAccuracy(String locEstimatedAccuracy) {
        this.locEstimatedAccuracy = locEstimatedAccuracy;
    }

}
