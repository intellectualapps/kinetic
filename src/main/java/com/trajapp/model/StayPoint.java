package com.trajapp.model;


import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


//@Entity
//@Table(name = "stay_point")
//@XmlRootElement
//@NamedQueries({
  //@NamedQuery(name = "Location.findByImeiAndDateRange", query = "SELECT l FROM Location l WHERE l.locPhoneImei = :locPhoneImei AND l.locDate BETWEEN :startDate AND :endDate ORDER BY l.locDate ASC")})
public class StayPoint {

    @Id
    @NotNull
    @Column(name = "locId")
    private String locId;    
    
    @NotNull
    @Column(name = "locPhoneImei")
    private String locPhoneImei;    
    
    @NotNull
    @Column(name = "locLongitude")
    private String locLongitude;
    
    @NotNull
    @Column(name = "locLatitude")
    private String locLatitude;
    
    @NotNull
    @Column(name = "locAltitude")
    private String locAltitude;
    
    @NotNull
    @Column(name = "locStartDateTime")
    private Timestamp locStartDateTime;
    
    @NotNull
    @Column(name = "locEndDateTime")
    private Timestamp locEndDateTime;
    
    @NotNull
    @Column(name = "locDuration")
    private Long locDuration;
    
    @NotNull
    @Column(name = "locEstimateAccuracy")
    private String locEstimatedAccuracy;

    public StayPoint() {

    }

    public String getLocId() {
        return locId;
    }

    public void setLocId(String locId) {
        this.locId = locId;
    }

    public String getLocPhoneImei() {
        return locPhoneImei;
    }

    public void setLocPhoneImei(String locPhoneImei) {
        this.locPhoneImei = locPhoneImei;
    }

    public String getLocLongitude() {
        return locLongitude;
    }

    public void setLocLongitude(String locLongitude) {
        this.locLongitude = locLongitude;
    }

    public String getLocLatitude() {
        return locLatitude;
    }

    public void setLocLatitude(String locLatitude) {
        this.locLatitude = locLatitude;
    }

    public String getLocAltitude() {
        return locAltitude;
    }

    public void setLocAltitude(String locAltitude) {
        this.locAltitude = locAltitude;
    }

    public Timestamp getLocStartDateTime() {
        return locStartDateTime;
    }

    public void setLocStartDateTime(Timestamp locStartDateTime) {
        this.locStartDateTime = locStartDateTime;
    }

    public Timestamp getLocEndDateTime() {
        return locEndDateTime;
    }

    public void setLocEndDateTime(Timestamp locEndDateTime) {
        this.locEndDateTime = locEndDateTime;
    }

    public Long getLocDuration() {
        return locDuration;
    }

    public void setLocDuration(Long locDuration) {
        this.locDuration = locDuration;
    }

    public String getLocEstimatedAccuracy() {
        return locEstimatedAccuracy;
    }

    public void setLocEstimatedAccuracy(String locEstimatedAccuracy) {
        this.locEstimatedAccuracy = locEstimatedAccuracy;
    }

}
