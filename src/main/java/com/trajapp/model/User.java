package com.trajapp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "user")
@XmlRootElement
@NamedQueries({
  @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.userId = :userId "),  
  @NamedQuery(name = "User.findByUsernameFirstAndLastName", query = "SELECT u FROM User u WHERE u.userId LIKE :userId OR u.userFirstname LIKE :userFirstname OR u.userSurname LIKE :userSurname")})

public class User implements Serializable{

    @Id
    @NotNull
    @Column(name = "userId")
    private String userId;
    
    @NotNull
    @Column(name = "userSurname")
    private String userSurname;
    
    @NotNull
    @Column(name = "userFirstname")
    private String userFirstname;
    
    @NotNull
    @Column(name = "userOthername")
    private String userOthername;
    
    @NotNull
    @Column(name = "userGender")
    private String userGender;
    
    @NotNull
    @Column(name = "userDesignation")
    private String userDesignation;
    
    @NotNull
    @Column(name = "userState")
    private String userState;
    
    @NotNull
    @Column(name = "userLga")
    private String userLga;
    
    @NotNull
    @Column(name = "userAddress")
    private String userAddress;
    
    @NotNull
    @Column(name = "userMobileNo")
    private String userMobileNo;
    
    @NotNull
    @Column(name = "userPhoneImei")
    private String userPhoneImei;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "userDate")
    private Date userDate;    

    @NotNull
    @Column(name = "userPassword")
    private String userPassword;
    
    public User(String userId, String userSurname, String userFirstname,
            String userOthername, String userGender, String userDesignation,
            String userState, String userLga, String userAddress, String userMobileNo,
            String userPhoneImei, Date userDate) {
        this.userId = userId;
        this.userSurname = userSurname;
        this.userFirstname = userFirstname;
        this.userOthername = userOthername;
        this.userGender = userGender;
        this.userDesignation = userDesignation;
        this.userState = userState;
        this.userLga = userLga;
        this.userAddress = userAddress;
        this.userMobileNo = userMobileNo;
        this.userPhoneImei = userPhoneImei;
        this.userDate = userDate;
    }

    public User() {
        
        userOthername = "";
        userGender = "";
        userDesignation = "";
        userState = "";
        userLga = "";
        userAddress = "";
        userPhoneImei = "";
        userDate = new Date();

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    public String getUserFirstname() {
        return userFirstname;
    }

    public void setUserFirstname(String userFirstname) {
        this.userFirstname = userFirstname;
    }

    public String getUserOthername() {
        return userOthername;
    }

    public void setUserOthername(String userOthername) {
        this.userOthername = userOthername;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserDesignation() {
        return userDesignation;
    }

    public void setUserDesignation(String userDesignation) {
        this.userDesignation = userDesignation;
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState;
    }

    public String getUserLga() {
        return userLga;
    }

    public void setUserLga(String userLga) {
        this.userLga = userLga;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUserMobileNo() {
        return userMobileNo;
    }

    public void setUserMobileNo(String userMobileNo) {
        this.userMobileNo = userMobileNo;
    }

    public String getUserPhoneImei() {
        return userPhoneImei;
    }

    public void setUserPhoneId(String userPhoneImei) {
        this.userPhoneImei = userPhoneImei;
    }

    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }
	
}
