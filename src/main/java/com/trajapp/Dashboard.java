package com.trajapp;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author auwalms
 */
public class Dashboard extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie[] cookies = request.getCookies();
        boolean authenticated = false;

        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                String name = cookies[i].getName();
                if (name.equals("humams-authToken")) {
                    authenticated = true;
                    break;
                }
            }
        }

        if (authenticated) {
            request.getRequestDispatcher("WEB-INF/pages/dashboard.html").forward(request, response);
        } else {
            response.sendRedirect("/humams/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Servlet to manage the dashboard page 'dashboard.html'";
    }

}