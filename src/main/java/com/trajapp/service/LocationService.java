/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.service;

import com.trajapp.manager.LocationManagerLocal;
import com.trajapp.pojo.LocationPayload;
import com.trajapp.pojo.LocationPayloadPayload;
import com.trajapp.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/location")
public class LocationService {

    @Context
    HttpServletRequest request;

    @EJB
    LocationManagerLocal locationManager;

    @GET
    @Path("/log")
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveLocation(@QueryParam("lphoneimei") String phoneImei,
            @QueryParam("llong") String longitude,
            @QueryParam("llat") String latitude,
            @QueryParam("lalt") String altitude,
            @QueryParam("ldate") String date,
            @QueryParam("ltime") String time,
            @QueryParam("lacc") String accuracy) throws GeneralAppException {

        Boolean isSaved = locationManager.saveLocation(phoneImei, longitude, latitude, altitude, date, time, accuracy);
        return Response.ok(isSaved).build();

    }
    
    @GET
    @Path("/points")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocationPoints(@QueryParam("ids") String ids,
            @QueryParam("from-date") String fromDate,
            @QueryParam("to-date") String toDate,
            @HeaderParam("Authorization") String rawToken) throws GeneralAppException {

        LocationPayloadPayload payload = locationManager.getLocationPoints(ids, fromDate, toDate, rawToken);
        return Response.ok(payload).build();

    }

}
