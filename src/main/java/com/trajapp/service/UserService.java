/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.service;


import com.trajapp.manager.UserManagerLocal;
import com.trajapp.pojo.AppUser;
import com.trajapp.pojo.UserPayload;
import com.trajapp.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/users")
public class UserService {
    
    @Context
    HttpServletRequest request;   
    
    @EJB    
    UserManagerLocal userManager;
       
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@QueryParam("email") String email,
                            @QueryParam("first-name") String firstName,
                            @QueryParam("surname") String surname,
                            @QueryParam("phone-number") String phoneNumber,
                            @QueryParam("password") String password) throws GeneralAppException {  
        
        AppUser appUser = userManager.register(email, firstName, surname, phoneNumber, password);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@HeaderParam("Authorization") String rawToken,
                            @PathParam("username") String username) throws GeneralAppException {  
        
        AppUser appUser = userManager.getUserDetails(username, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@PathParam("username") String username,
                                @QueryParam("email") String email,
                                @QueryParam("location") String location,
                                @QueryParam("first-name") String firstName,
                                @QueryParam("last-name") String lastName,
                                @QueryParam("photo-url") String photoUrl,
                                @QueryParam("phone-number") String phoneNumber,
                                @QueryParam("facebook-email") String facebookEmail,
                                @QueryParam("twitter-email") String twitterEmail,
                                @HeaderParam("Authorization") String rawToken) throws GeneralAppException {  
        
        AppUser appUser = userManager.updateUserDetails(username, email, firstName, lastName, phoneNumber, photoUrl, 
                                                        location, facebookEmail, twitterEmail, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/{username}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@PathParam("username") String username) throws GeneralAppException {  
        
        return Response.ok(userManager.deleteUser(username)).build();
           
    }
    
    @Path("/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticateUser(@QueryParam("id") String id,
                            @QueryParam("password") String password,
                            @QueryParam("auth-type") String type) throws GeneralAppException {  
        
        AppUser appUser = userManager.authenticate(id, password, type);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/verify/{username}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response verifyUsername(@PathParam("username") String username) throws GeneralAppException {   
        
        return Response.ok(userManager.verifyUsername(username)).build();            
    
    }

    @Path("/get-list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListOfUsers(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("usernames") String usernames) throws GeneralAppException {  
        
        UserPayload appUser = userManager.getListOfUsers(usernames, rawToken);
        return Response.ok(appUser).build();
           
    }
    
    @Path("/search-users")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchUsers(@HeaderParam("Authorization") String rawToken,
                            @QueryParam("query") String searchTerm,
                            @QueryParam("page-size") String pageSize,
                            @QueryParam("page-number") String pageNumber) throws GeneralAppException {  
        
        UserPayload appUser = userManager.searchUsers(searchTerm, pageNumber, pageSize, rawToken);
        return Response.ok(appUser).build();
           
    }
    
}
