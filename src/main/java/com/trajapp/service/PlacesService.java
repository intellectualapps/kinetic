/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trajapp.service;

import com.trajapp.manager.PlacesManagerLocal;
import com.trajapp.util.exception.GeneralAppException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/v1/place")
public class PlacesService {

    @Context
    HttpServletRequest request;

    @EJB
    PlacesManagerLocal placesManager;

    @GET
    @Path("/{latitude}/{longitude}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlaces(@PathParam("latitude") String latitude,
            @PathParam("longitude") String longitude) throws GeneralAppException {

        String places = placesManager.getPlaces(latitude, longitude);
        return Response.ok(places).build();

    }    

}
