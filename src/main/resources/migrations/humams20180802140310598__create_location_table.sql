CREATE TABLE `location` (
  `locId` VARCHAR(64) NOT NULL,
  `locPhoneImei` VARCHAR(20) NOT NULL,
  `locLatitude` VARCHAR(20) NOT NULL,
  `locLongitude` VARCHAR(20) NOT NULL,
  `locAltitude` VARCHAR(20) NOT NULL,
  `locDate` DATETIME NOT NULL,
  `locTime` DATETIME NOT NULL,
  `locEstimateAccuracy` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`locId`));