const BASEURL = "http://188.166.98.132:8080/humams/api/v1";
const authToken = getCookie("humams-authToken");
const name = getCookie("humams-name");
const email = getCookie("humams-email");

/**
 * Set Cookie
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

/**
 * Get Cookie
 */
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(";");
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 * Delete Cookie
 */
function deleteCookie(name) {
    setCookie(name, "", -1);
}

/**
 * Log out
 */
function logout() {
    deleteCookie("humams-name");
    deleteCookie("humams-authToken");
    deleteCookie("humams-email");
    window.location = "/humams";
}

document.querySelector(".logout-btn").addEventListener("click", logout);

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split("&"),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split("=");

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function toStandardTime(militaryTime) {
    militaryTime = militaryTime.split(":");
    if (militaryTime[0].charAt(0) == 1 && militaryTime[0].charAt(1) > 2) {
        return (
            militaryTime[0] -
            12 +
            ":" +
            militaryTime[1] +
            ":" +
            militaryTime[2] +
            " PM"
        );
    } else {
        return militaryTime.join(":") + " AM";
    }
}
