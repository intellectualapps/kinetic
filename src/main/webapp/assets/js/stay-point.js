var map,
    mapPointer,
    locationsData,
    staypointInfo,
    mapLineColor,
    staypointCordinates = [];

window.onload = () => {
    launchModal();
};
document.querySelector("#stayPoint").addEventListener("click", function() {
    launchModal();
});

function launchModal() {
    $("#staypointModal").modal("show");
}

function getParameters(event) {
    event.preventDefault();
    const Ids = document.querySelector("#userId").value;
    const dateFrom = document.querySelector("#date-from").value;
    const dateTo = document.querySelector("#date-to").value;
    let alertBox = document.querySelector("#alertBox");
    let colorArray = [
        "#21234c",
        "#ff0000",
        "#b030c7",
        "#0014ff",
        "#f2b4b4",
        "#28a745",
        "#721c24",
        "#fdf59a",
        "#856404",
        "#0c5460"
    ];

    if (dateTo == "" || dateFrom == "") {
        alert("Please provide a start and end dates");
        return;
    } else {
        let params = `ids=${Ids}&from-date=${dateFrom}&to-date=${dateTo}`;
        let URL = `${BASEURL}/location/points?${params}`;
        fetch(URL, {
            headers: {
                Authorization: "Bearer: " + authToken
            }
        })
            .then(response => response.json())
            .then(response => {
                if (response.status === 400) {
                    alertBox.innerHTML = response.message;
                    alertBox.classList = "text-center alert alert-danger";
                } else {
                    $("#staypointModal").modal("hide");
                    let responseData = response.payload;
                    responseData.forEach((cordinates, pointIndex) => {
                        locationsData = cordinates.stayPoints;
                        let lineColor = colorArray[pointIndex];
                        showPoints(locationsData, lineColor);
                        showInfoHTML(cordinates.locationPointDaySummary);
                    });
                }
            })
            .catch(error => {
                alertBox.innerHTML = error.message;
                alertBox.classList = "text-center alert alert-danger";
            });
    }
}

$(function() {
    $(`#date-from`).datetimepicker({
        format: "dd-MM-yyyy HH:ii:ss p",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
    $(`#date-to`).datetimepicker({
        format: "dd-MM-yyyy HH:ii:ss p",
        showMeridian: true,
        autoclose: true,
        todayBtn: true
    });
});

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        zoom: 13.74,
        center: { lat: 9.0580004, lng: 7.4626531 },
        mapTypeId: "terrain"
    });
}

function showPoints(locations, lineColor) {
    locations.forEach((location, locationIndex) => {
        if (mapLineColor !== lineColor) {
            mapLineColor = lineColor;
        }

        addMarker(
            {
                lat: parseFloat(location.locLatitude),
                lng: parseFloat(location.locLongitude)
            },
            location,
            locationIndex
        );
    });
}

function addPointInfo(locationDetails, index) {
    getLandUseDetails(locationDetails);
    let infoContent = `<div>
        <p class="h6">Point ${index + 1}</p>
        <p>${locationDetails.locStartDateTime.substring(0, 10)}</p>
        <p>${toStandardTime(
            locationDetails.locStartDateTime.substring(11, 19)
        )}</p>
    </div>`;

    staypointInfo = new google.maps.InfoWindow({
        content: infoContent
    });
}
// Adds a marker to the map.
function addMarker(location, locationDetails, index) {
    mapPointer = new google.maps.Marker({
        position: location,
        map: map
    });

    staypointCordinates.push(location);
    if (staypointCordinates.length == locationsData.length) {
        mapLine();
        staypointCordinates = [];
    }

    mapPointer.addListener("click", function() {
        addPointInfo(locationDetails, index);
        staypointInfo.open(map, mapPointer);
        showInfo();
    });
}
function mapLine() {
    var linePath = new google.maps.Polyline({
        path: staypointCordinates,
        geodesic: true,
        strokeColor: `${mapLineColor}`,
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    linePath.setMap(map);
}

function showInfo() {
    document.querySelector(".info-left").classList.remove("hidden");
    document.querySelector(".info-right").classList.remove("hidden");
    document.querySelector(".map").style.width = "50%";
}

function showInfoHTML(daySummary) {
    let pointCounter = 0;
    daySummary.forEach(day => {
        let infoBox = document.querySelector(".info-left");
        let infoDate = document.createElement("h6");
        infoDate.classList.add("point-header");
        infoDate.innerHTML = day.date;
        infoBox.appendChild(infoDate);

        day.daySummary.forEach(summary => {
            pointCounter++;
            let time = summary.substring(0, 11),
                timeSpent = summary.substring(42);
            let infoPoint = document.createElement("p");
            infoPoint.classList.add("point-info");
            infoPoint.innerHTML = `${time} - Point ${pointCounter} - ${timeSpent}`;
            infoBox.appendChild(infoPoint);
        });
    });
}

function getLandUseDetails(pointInfo) {
    let URL = `${BASEURL}/place/${pointInfo.locLatitude}/${
        pointInfo.locLongitude
    }`;
    fetch(URL, {
        headers: {
            Authorization: "Bearer: " + authToken
        }
    })
        .then(response => response.json())
        .then(response => {
            if (response.status !== 400) showPOIHTML(response);
        })
        .catch(error => console.error(error));
}

function showPOIHTML(details) {
    let dynamicPOIDiv = document.querySelector("#dynamic-poi");
    if (dynamicPOIDiv.hasChildNodes) {
        while (dynamicPOIDiv.firstChild) dynamicPOIDiv.firstChild.remove();
    }
    details.forEach(detail => {
        let pointOfInt = document.createElement("p");
        pointOfInt.classList.add("point-info");
        pointOfInt.innerHTML = detail.name;
        dynamicPOIDiv.appendChild(pointOfInt);
    });
}
