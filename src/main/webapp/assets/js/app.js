"Use Strict";
let alertBox = document.querySelector("#loginAlert");

onload = function() {
    document.querySelector("#loginForm").addEventListener("submit", event => {
        event.preventDefault();
        const userId = document.querySelector("#usernameInput").value;
        const password = document.querySelector("#passwordInput").value;
        if (!userId || !password) {
            alertBox.innerHTML = "Please provide username and password";
            alertBox.classList = "text-center alert alert-danger";
            return;
        }
        loginUser(userId, password);
    });
};

function loginUser(userId, password) {
    fetch(
        `${BASEURL}/users/authenticate?id=${userId}&password=${password}&auth-type=email`,
        {
            method: "POST"
        }
    )
        .then(response => response.json())
        .then(function(response) {
            if (!response.authToken) {
                alertBox.innerHTML = response.message;
                alertBox.classList = "text-center alert alert-danger";
            } else {
                console.log(response);
                setCookie("humams-email", response.userId);
                setCookie("humams-authToken", response.authToken);
                setCookie(
                    "humams-name",
                    `${response.userFirstname} ${response.userSurname}`
                );
                alertBox.innerHTML =
                    "Login Successfull, you will be redirect in a moment";
                alertBox.classList = "text-center alert alert-success";
                setTimeout(function() {
                    location = "/humams/dashboard";
                }, 2000);
            }
        })
        .catch(function(error) {
            console.log(error);
            alertBox.innerHTML = error.message;
            alertBox.classList = "text-center alert alert-danger";
        });
}
